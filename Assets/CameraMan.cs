﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMan : MonoBehaviour {
	const float ratio = 6.65f;
	Vector2 resolution;

	// Use this for initialization
	void Start () {
		forceCameraSize();
		this.resolution = new Vector2(Screen.width, Screen.height);
		
	}
	
	// Update is called once per frame
	void Update(){
		if (resolution.x != Screen.width || resolution.y != Screen.height){
			this.forceCameraSize();
			this.resolution.x = Screen.width;
			this.resolution.y = Screen.height;
		}
	}

	public void forceCameraSize(){
		this.gameObject.GetComponent<Camera>().orthographicSize = Mathf.Max(((float)CameraMan.ratio / ((float)Screen.width / (float)Screen.height)), (float)CameraMan.ratio * (float)3 / (float)4);
	}
}
