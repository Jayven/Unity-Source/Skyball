﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ContinueButton : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick (PointerEventData eventData) {
		Time.timeScale = 1;
		this.transform.parent.transform.parent.gameObject.SetActive(false);
	}
}