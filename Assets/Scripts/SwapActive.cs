﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapActive : MonoBehaviour {
	GameObject Controller;
	GameObject ScoreBlocks;
	public bool isActive = false;
	public bool used = false;

	void Start(){
		Controller = GameObject.Find("LevelController");
		ScoreBlocks = GameObject.Find("Score-Blocks").transform.GetChild(0).gameObject;
		updateBlockScore();
	}
	void OnMouseDown(){
		isActive = !isActive;
		this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = !isActive;
		this.gameObject.transform.GetChild(0).gameObject.SetActive(isActive);
		this.gameObject.transform.GetChild(1).gameObject.SetActive(!isActive);
		if (!used){
			used = true;
			Controller.GetComponent<LevelController>().blocksUsed++;
			updateBlockScore();
			Controller.GetComponent<LevelController>().checkforVictory();
		}
	}
	void updateBlockScore(){
		ScoreBlocks.GetComponent<Text>().text = "Used Blocks: " + Controller.GetComponent<LevelController>().blocksUsed;
		
		if (Controller.GetComponent<LevelController>().VictoryByBlocks)
			ScoreBlocks.GetComponent<Text>().text += "/" + Controller.GetComponent<LevelController>().maxBlocks;
	}
}