﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {
	public static int Level = 1;

	public GameObject menu;

	public bool VictoryByBalls;
	public bool VictoryByGoals;
	public bool VictoryByBlocks;

	public int maxBalls;
	public int maxGoals;
	public int maxBlocks;

	public int openBalls;
	public int openGoals;
	public int blocksUsed;

	public void checkforVictory(){
		bool win = true;
		if (VictoryByBalls && openBalls > maxBalls)
				win = false;
		if (VictoryByGoals && openGoals > maxGoals)
				win = false;
		if (VictoryByBlocks && blocksUsed > maxBlocks)
				win = false;
		if (win){
			menu.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
			menu.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
			menu.transform.GetChild(1).transform.GetChild(2).gameObject.SetActive(false);
			menu.transform.GetChild(1).transform.GetChild(4).gameObject.SetActive(true);
			menu.transform.GetChild(1).transform.GetChild(5).gameObject.SetActive(true);
			menu.SetActive(true);
		}
	}

	public void resetLevel(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}
}
