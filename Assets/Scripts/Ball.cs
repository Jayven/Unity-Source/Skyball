﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {
	GameObject Controller;
	GameObject ScoreGoals;
	GameObject ScoreBalls;

	void Start(){
		Controller = GameObject.Find("LevelController");
		Controller.GetComponent<LevelController>().openBalls++;
			
		ScoreGoals = GameObject.Find("Score-Goals").transform.GetChild(0).gameObject;
		ScoreBalls = GameObject.Find("Score-Balls").transform.GetChild(0).gameObject;
		
		ScoreBalls.GetComponent<Text>().text = "Open Balls: " + Controller.GetComponent<LevelController>().openBalls;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Goal"){
			Controller.GetComponent<LevelController>().openGoals--;

			ScoreGoals.GetComponent<Text>().text = "Open Goals: " + Controller.GetComponent<LevelController>().openGoals;
		
			Controller.GetComponent<LevelController>().checkforVictory();
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Goal"){
			Controller.GetComponent<LevelController>().openGoals++;

			ScoreGoals.GetComponent<Text>().text = "Open Goals: " + Controller.GetComponent<LevelController>().openGoals;
		
			Controller.GetComponent<LevelController>().checkforVictory();
		}
	}
}
