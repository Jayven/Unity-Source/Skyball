﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour {
	GameObject Controller;
	GameObject ScoreBalls;
	GameObject ScoreGoals;

	void Start(){
		Controller = GameObject.Find("LevelController");
		Controller.GetComponent<LevelController>().openGoals++;

		ScoreBalls = GameObject.Find("Score-Balls").transform.GetChild(0).gameObject;
		ScoreGoals = GameObject.Find("Score-Goals").transform.GetChild(0).gameObject;
	
		ScoreGoals.GetComponent<Text>().text = "Open Goals: " + Controller.GetComponent<LevelController>().openGoals;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Ball"){
			Controller.GetComponent<LevelController>().openBalls--;
			
			ScoreBalls.GetComponent<Text>().text = "Open Balls: " + Controller.GetComponent<LevelController>().openBalls;

			Controller.GetComponent<LevelController>().checkforVictory();
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Ball"){
			Controller.GetComponent<LevelController>().openBalls++;

			ScoreBalls.GetComponent<Text>().text = "Open Balls: " + Controller.GetComponent<LevelController>().openBalls;
		
			Controller.GetComponent<LevelController>().checkforVictory();
		}
	}
}
