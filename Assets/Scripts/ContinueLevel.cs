﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ContinueLevel : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick (PointerEventData eventData) {
		Time.timeScale = 1;

		SceneManager.LoadScene(LevelController.Level, LoadSceneMode.Single);

		Time.timeScale = 1;
	}
}