﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class NextLevelButton : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick (PointerEventData eventData) {
		Time.timeScale = 0;

		int levelIndex = SceneManager.GetActiveScene().buildIndex;

		LevelController.Level = levelIndex + 1;

		SceneManager.LoadScene(levelIndex + 1, LoadSceneMode.Single);

		Time.timeScale = 1;
	}
}