﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ResetButton : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick (PointerEventData eventData) {
		GameObject.Find("LevelController").GetComponent<LevelController>().resetLevel();
	}
}
