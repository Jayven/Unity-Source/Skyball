﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenuButton : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick (PointerEventData eventData) {
		Time.timeScale = 1;
		//this.transform.parent.transform.parent.gameObject.SetActive(false);
		SceneManager.LoadScene(0, LoadSceneMode.Single);
	}
}