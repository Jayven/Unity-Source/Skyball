﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelMarker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Level: " + SceneManager.GetActiveScene().buildIndex;
	}
}
